/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipos.shapeproject;

/**
 *
 * @author BOAT
 */
public class TestRectangle {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3, 4);
        System.out.println("Area of rectangle1((h,w) = " + rectangle1.getR() + "," + rectangle1.getR1() + " ) is " + rectangle1.squareArea());
        rectangle1.setR(4, 5);
        System.out.println("Area of rectangle1((h,w) = " + rectangle1.getR() + "," + rectangle1.getR1() + " ) is " + rectangle1.squareArea());
        rectangle1.setR(0, 0);
        System.out.println("Area of rectangle1((h,w) = " + rectangle1.getR() + "," + rectangle1.getR1() + " ) is " + rectangle1.squareArea());
    }
}
