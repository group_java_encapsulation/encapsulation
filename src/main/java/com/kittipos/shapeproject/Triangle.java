/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipos.shapeproject;

/**
 *
 * @author BOAT
 */
public class Triangle {

    double b;
    double h;
    final double tr = b * h / 2;

    public Triangle(double b, double h) {
        this.b = b;
        this.h = h;
    }

    public double triangleArea() {
        return b * h / 2;
    }

    public double getR() {
        return b;
    }

    public double getR1() {
        return h;
    }

    public void setR(double b, double h) {
        if (b <= 0 && h <= 0) {
            System.out.println("Error: Radius must more than zero!!!");
            return;
        }
        this.b = b;
        this.h = h;

    }
}
