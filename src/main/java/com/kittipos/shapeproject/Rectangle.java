/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipos.shapeproject;

/**
 *
 * @author BOAT
 */
public class Rectangle {

    private double h;
    private double w;
    final double eq = h * w;

    public Rectangle(double h, double w) {
        this.h = h;
        this.w = w;
    }

    public double squareArea() {
        return h * w;
    }

    public double getR() {
        return h;

    }

    public double getR1() {
        return w;
    }

    public void setR(double h, double w) {
        if (h <= 0 && w <= 0) {
            System.out.println("Error: Radius must more than zero!!!");
            return;
        }
        this.h = h;
        this.w = w;

    }
}
