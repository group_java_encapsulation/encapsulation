/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipos.shapeproject;

/**
 *
 * @author BOAT
 */
public class TestSquare {
    public static void main(String[] args) {
        Square square1 = new Square(2);
        System.out.println("Area of Square(s = " + square1.getR() + " ) is " + square1.squareArea());
        square1.setR(4);
        System.out.println("Area of Square(s = " + square1.getR() + " ) is " + square1.squareArea());
        square1.setR(0);
        System.out.println("Area of Square(s = " + square1.getR() + " ) is " + square1.squareArea());
    }
}
